# Project Name - val_description
# Current Maintainers: 
    - Misha Savchenko - misha.savchenko@nasa.gov
# Previous Maintainers: 
    - Jordan Lack - jordan.t.lack@nasa.gov
    - Frank Mathis frank.b.mathis@nasa. 


# Description
The val_description package contains the files needed to generate an URDF for Valkyrie as well as the meshes for visualization. Also contained in the val_description are xacro files for generating Valkyrie instance files. Valkyrie instance files are xml files that contain information about the hardware(actuators, shared memory nodes, sensors, etc). Some of this information is shared between the URDF and instance files. With this, the various coefficient files are included.

The URDF's are generated when you `catkin build` a Catkin workspace that contains val_description. 

# Notes

* Valkyrie is a development platform, and it has gone through a lot of software and hardware changes. One of the goals for maintaining this repo is to create a flexible enough system of xacros that allows for fast iteration on the existing characteristics of the robot, as well as on any hardware and software that might be adapted to it.
* The primary files that help with the understanding how valkyrie instances are constructed are `model/robots/valkyrie_generic.xacro` and `model/robots/common/xacro/valkyrie_base_hw.xacro`.


---

#### Credits
- Johnson Space Center - ER4 - Valkyrie Team
- IHMC humanoids group
- Maurice Fallon and his group
- Open Source Robotics Foundation(OSRF)

#### License
- NASA-1.3