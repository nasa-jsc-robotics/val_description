# Right and left arms

LEFT_FOREARMS = [{"name": "ValALeft", "j5": "v_g_018", "forearm": "v_h_007"},
                 {"name": "ValBLeft", "j5": "v_g_016", "forearm": "v_h_005"},
                 {"name": "ValCLeft", "j5": "v_g_005", "forearm": "v_h_001"},
                 {"name": "ValDLeft", "j5": "v_g_021", "forearm": "v_h_003"},
                 {"name": "ValELeft", "j5": "v_g_016", "forearm": "v_h_005"}]

RIGHT_FOREARMS = [{"name": "ValARight", "j5": "v_g_017", "forearm": "v_h_008"},
                  {"name": "ValBRight", "j5": "v_g_012", "forearm": "v_h_006"},
                  {"name": "ValCRight", "j5": "v_g_003", "forearm": "v_h_002"},
                  {"name": "ValDRight", "j5": "v_g_020", "forearm": "v_h_004"},
                  {"name": "ValERight", "j5": "v_g_012", "forearm": "v_h_006"}]
