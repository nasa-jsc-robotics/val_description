#!/bin/env python3
import sys
import rospy
import rospkg
import xacro

from dynamic_reconfigure.server import Server
from val_description.cfg import ForearmSwapConfig
from val_description.ForearmNames import LEFT_FOREARMS, RIGHT_FOREARMS

ROBOTS = ["valkyrie_A", "valkyrie_E", "valkyrie_B"]

class ForearmSwapper:
    shared_description_dir = None

    robot = None
    left_forearm_name = None
    left_j5 = None
    left_forearm = None
    
    right_j5 = None
    right_forearm = None
    
    dyn_rec_srv = None

    last_update_instance = False

    def __init__(self):
        rospy.logdebug("Look ma' no hands")
        self.shared_description_dir = rospkg.RosPack().get_path("val_description")
        self.dyn_rec_srv = Server(ForearmSwapConfig, self.callback)

    def callback(self, config, level):
        self.robot = ROBOTS[config["robots"]]
        self.left_forearm_name, self.left_j5, self.left_forearm = LEFT_FOREARMS[config["left_forearm"]].values()
        self.right_forearm_name, self.right_j5, self.right_forearm = RIGHT_FOREARMS[config["right_forearm"]].values()

        rospy.logdebug("\nSetting the Left Forearm to use %s."\
                      "\n\tJ5      : %s"\
                      "\n\tForearm : %s", self.left_forearm_name, self.left_j5, self.left_forearm)
        rospy.logdebug("\nSetting the Right Forearm to use %s."\
                      "\n\tJ5      : %s"\
                      "\n\tForearm : %s", self.right_forearm_name, self.right_j5, self.right_forearm)

        if config["update_instances"] != self.last_update_instance: 
            self.get_new_instance()

        self.last_update_instance = config["update_instances"]
        return config

    def get_new_instance(self):
        robot_arg = self.shared_description_dir + "/instance/instances/robots/" + self.robot + ".xacro"
        right_forearm_arg = "right_forearm:="+self.right_forearm_name
        left_forearm_arg = "left_forearm:="+self.left_forearm_name

        output_arg = self.shared_description_dir + "/instance/instances/instance_files/"+self.robot +".xml"
        sys.argv[1:] = [robot_arg, right_forearm_arg, left_forearm_arg, "-o", output_arg]
        xacro.main()
    
if __name__ == "__main__":
    rospy.init_node("forearm_swapper", anonymous = False, log_level=rospy.DEBUG)
    forearm_swapper = ForearmSwapper()

    rospy.spin()