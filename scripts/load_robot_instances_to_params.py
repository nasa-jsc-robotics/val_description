#!/bin/env python

from logging import exception
import rospy
import argparse
import sys
import xmltodict




def load_params_to_server(xml_params_dict, prefix):
    """Load parameters from an instance file to the parameter server under the given prefix.

    Args:
        xml_params_dict (str): path to the robot instance file
        prefix (str): rosparam prefix to use for instance parameters
    """

    try:
        rospy.set_param(prefix, xml_params_dict)
    except rospy.ROSException as e:
        rospy.logerr("Unable to set instance parameters on the param server: " + str(e))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    input_group = parser.add_mutually_exclusive_group(required=True)
    input_group.add_argument("-i", "--instance-file", help="Path to an XML instance file")
    input_group.add_argument("-p", "--parameter", help="Path to an XML instance file")
    
    parser.add_argument("-n", "--namespace", default="/valkyrie/instance", help="Parameter server prefix for instance params")
    args, unknown = parser.parse_known_args()
    
    if args.instance_file: 
        try:
            xml_params = open(instance_file).read()
            xml_params_dict = xmltodict.parse(xml_params, attr_prefix='', cdata_key='text', dict_constructor=dict)
        except Exception as e:
            rospy.logerr("Unable to load XML instance file: " + str(e))
            sys.exit(1)

    elif args.parameter:
        rospy.loginfo("Reading from parameter service variable [ " + args.parameter + " ]" )
        try:
            xml_params_dict = xmltodict.parse(rospy.get_param(args.parameter), attr_prefix='', cdata_key='text', dict_constructor=dict)
        except IOError as e:
            rospy.logerr("Couldn't load Instance: " + e + " from parameter server")
            sys.exit(1)

    else: 
        rospy.logfatal("I'm not sure what happened")
        sys.exit(1)
    rospy.init_node(name="load_params_to_server", anonymous=True)
    load_params_to_server(xml_params_dict, args.namespace)
